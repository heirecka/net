# Copyright 2009-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake [ api=2 ] freedesktop-desktop freedesktop-mime gtk-icon-cache \
    option-renames [ renames=[ 'qt qt5' ] ]

export_exlib_phases src_prepare src_configure src_install pkg_postinst pkg_postrm

SUMMARY="network protocol analyzer, formerly known as Ethereal"
DESCRIPTION="
Wireshark (formerly Ethereal) is a network protocol analyzer, or packet sniffer,
that lets you capture and interactively browse the contents of network frames.
The goal of the project is to create a commercial-quality packet analyzer for
Unix, and the most useful packet analyzer on any platform.
"
HOMEPAGE="https://www.${PN}.org"
DOWNLOADS="${HOMEPAGE}/download/src/all-versions/${PN}-${PV/-}.tar.xz"

BUGS_TO=""
REMOTE_IDS="freecode:${PN}"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/docs"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    c-ares    [[ description = [ Use the GNU c-ares library to resolve DNS names ] ]]
    caps
    geoip     [[ description = [ Enable support for GeoIP location data ] ]]
    gnutls
    gtk       [[ description = [ Build the GTK+ GUI (GTK+2 or GTK+3 based on selected provider) ] ]]
    kerberos
    lua       [[ description = [ Enable support for LUA scripting ] ]]
    lz4       [[ description = [ LZ4 lossless compression algorithm is used in some protocol (CQL...) ] ]]
    portaudio [[ description = [ Build the rtp_player for the GTK GUI (uses PortAudio) ] ]]
    qt5       [[ description = [ Build the Qt 5 GUI and the rtp_player (uses qtmultimedia) ] ]]
    snappy    [[ description = [ Snappy decompression in CQL and Kafka dissectors ] ]]
    smi       [[ description = [ Use libsmi to resolve numeric OIDs into human readable format ] ]]
    gtk? ( ( providers: gtk2 gtk3 ) [[ number-selected = exactly-one ]] )
    portaudio? ( ( providers: gtk2 gtk3 ) [[ number-selected = exactly-one ]] )
"

DEPENDENCIES="
    build:
        app-doc/asciidoc
        app-text/docbook-xml-dtd:4.5
        app-text/docbook-xsl-stylesheets
        dev-libs/libxslt [[ note = [ xsltproc ] ]]
        sys-devel/bison
        sys-devel/flex
        virtual/pkg-config[>=0.15.0]
        qt5? ( x11-libs/qttools:5[>=5.8] )
    build+run:
        dev-libs/glib:2[>=2.22.0]
        dev-libs/libgcrypt[>=1.4.2]
        dev-libs/libgpg-error
        dev-libs/libpcap
        net-libs/libssh[>=0.6]
        net-libs/libnl:3.0
        net-libs/nghttp2
        dev-libs/libxml2:2.0
        dev-perl/Parse-Yapp [[ note = [ HTTP header TPG plugin ] ]]
        group/${PN}
        sys-libs/zlib
        caps? ( sys-libs/libcap )
        c-ares? ( net-dns/c-ares[>=1.5] )
        geoip? ( net-libs/GeoIP )
        gnutls? ( dev-libs/gnutls[>=3.1.10] )
        lz4? ( app-arch/lz4 )
        providers:gtk2? (
            x11-libs/cairo
            x11-libs/gdk-pixbuf:2.0
            x11-libs/gtk+:2[>=2.12]
            x11-libs/pango
        )
        providers:gtk3? (
            x11-libs/cairo
            x11-libs/gdk-pixbuf:2.0
            x11-libs/gtk+:3[>=3.0.0]
            x11-libs/pango
        )
        kerberos? ( virtual/kerberos )
        lua? ( dev-lang/lua:=[>=5.1&<5.3] )
        portaudio? ( media-libs/portaudio )
        qt5? (
            x11-libs/qtbase:5[>=5.8][gui]
            x11-libs/qtmultimedia:5[>=5.8]
        )
        smi? ( net-libs/libsmi )
        snappy? ( app-arch/snappy )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_INSTALL_DATADIR:PATH=/usr/share
    -DCMAKE_INSTALL_MANDIR:PATH=/usr/share/man
    -DBUILD_androiddump:BOOL=TRUE
    -DBUILD_capinfos:BOOL=TRUE
    -DBUILD_captype:BOOL=TRUE
    -DBUILD_ciscodump:BOOL=TRUE
    -DBUILD_dftest:BOOL=TRUE
    -DBUILD_randpkt{,dump}:BOOL=TRUE
    -DBUILD_sharkd:BOOL=TRUE
    -DBUILD_sshdump:BOOL=TRUE
    -DBUILD_text2pcap:BOOL=TRUE
    -DBUILD_{dump,edit,merge,reorder}cap:BOOL=TRUE
    -DBUILD_{raw,tf,t}shark:BOOL=TRUE
    -DBUILD_udpdump:BOOL=TRUE
    -DENABLE_ASAN:BOOL=FALSE
    -DENABLE_AIRPCAP:BOOL=FALSE
    -DENABLE_APPLICATION_BUNDLE:BOOL=FALSE
    -DENABLE_CCACHE:BOOL=FALSE
    -DENABLE_CHECKHF_CONFLICT:BOOL=FALSE
    -DENABLE_CHM_GUIDES:BOOL=FALSE
    -DENABLE_EXTCAP:BOOL=TRUE
    -DENABLE_EXTRA_COMPILER_WARNINGS:BOOL=FALSE
    -DENABLE_HTML_GUIDES:BOOL=FALSE
    -DENABLE_LIBXML2:BOOL=TRUE
    -DENABLE_NETLINK:BOOL=TRUE
    -DENABLE_PCAP:BOOL=TRUE
    -DENABLE_PCAP_NG_DEFAULT:BOOL=TRUE
    -DENABLE_PDF_GUIDES:BOOL=FALSE
    -DENABLE_PLUGIN_IFDEMO:BOOL=FALSE
    -DENABLE_PLUGINS:BOOL=TRUE
    -DENABLE_SBC:BOOL=FALSE
    -DENABLE_SPANDSP:BOOL=FALSE
    -DENABLE_STATIC:BOOL=FALSE
    -DENABLE_UBSAN:BOOL=FALSE
    -DENABLE_WINSPARKLE:BOOL=FALSE
    -DENABLE_ZLIB:BOOL=TRUE
    -DWANT_PACKET_EDITOR:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    "c-ares CARES"
    "caps CAP"
    "geoip GEOIP"
    "gnutls GNUTLS"
    "kerberos KERBEROS"
    "lua LUA"
    "lz4 LZ4"
    "portaudio PORTAUDIO"
    "qt5 QT5"
    "smi SMI"
    "snappy SNAPPY"
)

wireshark_src_prepare() {
    # TODO: report upstream
    edo sed \
        -e 's:${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_DATADIR}/${CPACK_PACKAGE_NAME}:/usr/share/wireshark:g' \
        -i "${CMAKE_SOURCE}"/cmakeconfig.h.in

    cmake_src_prepare
}

wireshark_src_configure() {
    local cmakeargs=(
        "${CMAKE_SRC_CONFIGURE_PARAMS[@]}" \
        $(for s in "${CMAKE_SRC_CONFIGURE_OPTIONS[@]}" ; do
            cmake_option ${s}
        done )
        $(for s in "${CMAKE_SRC_CONFIGURE_OPTION_ENABLES[@]}" ; do
            cmake_enable ${s}
        done )
    )

    if option gtk ; then
        cmakeargs+=(
            -DBUILD_wireshark_gtk:BOOL=TRUE
            -DENABLE_GTK3:BOOL=$(option providers:gtk3 && echo TRUE || echo FALSE)
        )
    else
        cmakeargs+=( -DBUILD_wireshark_gtk:BOOL=FALSE )
    fi

    if option qt5 ; then
        cmakeargs+=( -DBUILD_wireshark:BOOL=TRUE )
    else
        cmakeargs+=( -DBUILD_wireshark:BOOL=FALSE )
    fi

    ecmake "${cmakeargs[@]}"
}

wireshark_src_install() {
    cmake_src_install

    edo chown 0:wireshark "${IMAGE}"/usr/$(exhost --target)/bin/tshark
    edo chmod 6550 "${IMAGE}"/usr/$(exhost --target)/bin/tshark
    edo chown 0:wireshark "${IMAGE}"/usr/$(exhost --target)/bin/dumpcap
    edo chmod 6550 "${IMAGE}"/usr/$(exhost --target)/bin/dumpcap

    insinto /usr/$(exhost --target)/include/wiretap
    doins "${CMAKE_SOURCE}"/wiretap/wtap.h

    dodoc \
        "${CMAKE_SOURCE}"/doc/randpkt.txt \
        "${FILES}"/README.Exherbo

    edo rm "${IMAGE}"/usr/$(exhost --target)/lib/wireshark/*.cmake
}

wireshark_pkg_postinst() {
    if option gtk || option qt5 ; then
        freedesktop-desktop_pkg_postinst
        freedesktop-mime_pkg_postinst
        gtk-icon-cache_pkg_postinst
    fi
}

wireshark_pkg_postrm() {
    if option gtk || option qt5 ; then
        freedesktop-desktop_pkg_postrm
        freedesktop-mime_pkg_postrm
        gtk-icon-cache_pkg_postrm
    fi
}

