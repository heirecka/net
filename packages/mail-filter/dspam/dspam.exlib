# Copyright 2010-2015 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'dspam-3.8.0-r15.ebuild' from Gentoo, which is:
#     Copyright 1999-2009 Gentoo Foundation

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.11 ] ] systemd-service

export_exlib_phases pkg_pretend src_prepare src_configure src_install

SUMMARY="DSPAM is a scalable and open-source content-based spam filter designed for multi-user enterprise systems"
DESCRIPTION="
DSPAM is a server-side statistical anti-spam agent for Unix email servers. It
masquerades as the email server's local delivery agent and effectively filters
spam using a combination of de-obfuscation techniques, specialized algorithms,
and statistical analysis. The result is an administratively maintenance-free,
self-learning anti-spam tool. DSPAM has yielded real-world success rates beyond
99.9% accuracy with less than a 0.01% chance of false positives.
"
HOMEPAGE="http://${PN}.sourceforge.net"
DOWNLOADS="mirror://sourceforge/${PN}/${PNV}.tar.gz"

BUGS_TO="philantrop@exherbo.org"
REMOTE_IDS="freecode:${PN}"

SF_GIT_URI="http://${PN}.git.sourceforge.net/git/gitweb.cgi?p=${PN}/${PN}"
UPSTREAM_CHANGELOG="${SF_GIT_URI};a=blob_plain;f=CHANGELOG;hb=RELENG_$(ever major)_$(ever range 2)_$(ever range 3)"
UPSTREAM_RELEASE_NOTES="${SF_GIT_URI};a=blob_plain;f=RELEASE.NOTES;hb=RELENG_$(ever major)_$(ever range 2)_$(ever range 3)"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    clamav        [[ description = [ Use clamav for virus scanning ] ]]
    debug         [[ description = [ Enable debug messages ] ]]
    mysql
    postgresql
    syslog        [[ description = [ Enable support for using syslog instead of a log file ] ]]
    user-homedirs [[ description = [ Use the users' home dirs for opt-in/opt-out ] requires = -virtual-users ]]
    virtual-users [[ description = [ Enable support for virtual users using an SQL backend ] requires = -user-homedirs ]]
    virtual-users? ( ( mysql postgresql ) [[ number-selected = at-least-one ]] )
"
# If ldap (external-lookup) is enabled, zlib isn't found anymore.
#    ldap

DEPENDENCIES="
    build+run:
        user/dspam
        group/dspam
        mysql? ( dev-db/mysql[>=5.1] )
        postgresql? ( dev-db/postgresql[>=8.3.7] )
    run:
        virtual/cron
        clamav? ( app-antivirus/clamav[>=0.94.2] )
        syslog? ( virtual/syslog )
    suggestion:
        app-admin/logrotate [[ description = [ Use logrotate for rotating logs ] ]]
"
# cf. MYOPTIONS
#        ldap? ( net-directory/openldap[>=2.4.16] )

DSPAM_HOMEDIR="/var/spool/dspam"
DSPAM_CONFDIR="/etc/mail/dspam"
DSPAM_LOGDIR="/var/log/dspam"
DSPAM_MODE=2511

DEFAULT_SRC_COMPILE_PARAMS="-j1"

dspam_pkg_pretend() {
    if [[ -f "${ROOT}"/etc/tmpfiles.d/${PN}.conf ]] ; then
        ewarn "The configuration file /etc/tmpfiles.d/${PN}.conf has been moved to"
        ewarn "/usr/$(exhost --target)/lib/tmpfiles.d/${PN}.conf and can be safely removed after upgrade"
        ewarn "if you did not make any changes to it."
    fi
}

dspam_src_prepare() {
    AT_M4DIR=( "${WORK}/m4" )
    eautoreconf
}

dspam_src_configure() {
    local myconf=""
    local STORAGE="hash_drv"

    option syslog || myconf="${myconf} --with-logfile=${DSPAM_LOGDIR}/dspam.log"

    # select storage driver
    if option mysql; then
        STORAGE="${STORAGE},mysql_drv"
        myconf="${myconf} --with-mysql-includes=/usr/$(exhost --target)/include/mysql"
        myconf="${myconf} --with-mysql-libraries=/usr/$(exhost --target)/lib/mysql"
    fi

    if option postgresql ; then
        STORAGE="${STORAGE},pgsql_drv"
        myconf="${myconf} --with-pgsql-includes=/usr/$(exhost --target)/include/postgresql"
        myconf="${myconf} --with-pgsql-libraries=/usr/$(exhost --target)/lib/postgresql"
    fi

    econf \
        --sysconfdir="${DSPAM_CONFDIR}" \
        --disable-external-lookup \
        --enable-daemon \
        --enable-large-scale \
        --enable-long-usernames \
        --enable-preferences-extension \
        --enable-split-configuration \
        --with-dspam-group=dspam \
        --with-dspam-home="${DSPAM_HOMEDIR}" \
        --with-dspam-home-group=dspam \
        --with-logdir="${DSPAM_LOGDIR}" \
        --with-dspam-mode=${DSPAM_MODE} \
        --with-storage-driver=${STORAGE} \
        $(option_enable clamav) \
        $(option_enable debug) \
        $(option_enable syslog) \
        ${myconf}
}
# cf. MYOPTIONS
#        $(option_enable ldap external-lookup) \

dspam_src_install () {
    diropts -m0770 -o dspam -g dspam
    dodir "${DSPAM_CONFDIR}"
    insinto "${DSPAM_CONFDIR}"
    insopts -m640 -o dspam -g dspam
    doins src/dspam.conf

    default

    # necessary for dovecot-dspam
    insopts -m644
    insinto /usr/$(exhost --target)/include/dspam && doins src/pref.h

    # create logdir
    diropts -m0770 -o dspam -g dspam
    keepdir "${DSPAM_LOGDIR}"

    diropts -m0755
    insinto /etc/logrotate.d
    newins "${FILES}"/logrotate.dspam dspam

    # We use sockets for the daemon instead of tcp port 24
    edo \
        sed -e 's:^#*\(ServerDomainSocketPath[\t ]\{1,\}\).*:\1\"/run/dspam/dspam.sock\":gI' \
            -e 's:^#*\(ServerPID[\t ]\{1,\}\).*:\1/run/dspam/dspam.pid:gI' \
            -e 's:^#*\(ClientHost[\t ]\{1,\}\)/.*:\1\"/run/dspam/dspam.sock\":gI' \
            -i "${IMAGE}/${DSPAM_CONFDIR}"/dspam.conf

    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins ${PN}.conf <<EOF
d /run/dspam 0775 root dspam
EOF
    install_systemd_files

    edo chown root:dspam "${IMAGE}"/usr/$(exhost --target)/bin/dspamc
    edo chmod u=rx,g=xs,o=x "${IMAGE}"/usr/$(exhost --target)/bin/dspamc

    # database related configuration and scripts
    insinto "${DSPAM_CONFDIR}"
    local PASSWORD="${RANDOM}${RANDOM}${RANDOM}${RANDOM}" DSPAM_DB_DATA=()
    if option mysql; then
        doins src/tools.mysql_drv/mysql_objects-4.1.sql
        newins src/tools.mysql_drv/purge.sql mysql_purge.sql
        newins src/tools.mysql_drv/purge-4.1.sql mysql_purge-4.1.sql
        if option virtual-users ; then
            newins src/tools.mysql_drv/virtual_users.sql mysql_virtual_users.sql
            newins src/tools.mysql_drv/virtual_user_aliases.sql mysql_virtual_user_aliases.sql
        fi
    fi
    if option postgresql ; then
        doins src/tools.pgsql_drv/pgsql_objects.sql
        newins src/tools.pgsql_drv/purge.sql pgsql_purge.sql
        if option virtual-users ; then
            newins src/tools.pgsql_drv/virtual_users.sql pgsql_virtual_users.sql
        fi

        # Install psycopg scripts needed when postgresql is not installed
        exeinto "${DSPAM_CONFDIR}"
        doexe "${FILES}"/pgsql_{createdb,purge}.py
    fi

    # Set default storage
    local DEFAULT_STORAGE
    if option mysql ; then
        DEFAULT_STORAGE=mysql
    elif option postgresql ; then
        DEFAULT_STORAGE=pgsql
    fi
    if [[ -z ${DEFAULT_STORAGE} ]]; then
        # When only one storage driver is compiled, it is linked statically with dspam
        # thus you should not set the StorageDriver at all
        # Also, hash_drv requires certain tokenizer and PValue (see bug #185718)
        edo \
            sed -e "s:^\(StorageDriver[\t ].*\)$:#\1:" \
                -e "s:^Tokenizer .*$:Tokenizer sbph:" \
                -e "/^#PValue/d" \
                -e "s:^PValue .*$:PValue markov:" \
                -i "${IMAGE}/${DSPAM_CONFDIR}"/dspam.conf
    else
        # Set the storage driver and use purge configuration for SQL-based installations
        edo \
            sed -e "s:^\(Purge.*\):###\1:g" \
                -e "s:^#\(Purge.*\):\1:g" \
                -e "s:^###\(Purge.*\):#\1:g" \
                -e "s:^\(StorageDriver[\t ].*\)libhash_drv.so:\1lib${DEFAULT_STORAGE}_drv.so:" \
                -i "${IMAGE}/${DSPAM_CONFDIR}"/dspam.conf
    fi

    # installs the notification messages
    # -> The documentation is wrong! The files need to be in ./txt
    echo "Scanned and tagged as SPAM with DSPAM ${PV} by Your ISP.com">"${TEMP}"/msgtag.spam
    echo "Scanned and tagged as non-SPAM with DSPAM ${PV} by Your ISP.com">"${TEMP}"/msgtag.nonspam
    insinto "${DSPAM_CONFDIR}"/txt
    doins "${WORK}"/txt/*.txt
    doins "${TEMP}"/msgtag.*

    # Create the opt-in / opt-out directories
    diropts -m0770 -o dspam -g dspam
    dodir "${DSPAM_HOMEDIR}"
    keepdir "${DSPAM_HOMEDIR}"/opt-in
    keepdir "${DSPAM_HOMEDIR}"/opt-out
    diropts -m0755

    # dspam cron job
    exeinto /etc/cron.daily
    doexe "${FILES}"/dspam.cron

    # Install a sendmail mailer definition as part of the docs.
    dodoc "${FILES}"/dspam.m4

    # documentation
    docinto doc
    dodoc doc/*.txt
    doman man/dspam*

    keepdir /usr/$(exhost --target)/lib/dspam
}

