# Copyright 2009-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'MailScanner-4.70.7.1.ebuild' from Gentoo, which is:
#     Copyright 1999-2009 Gentoo Foundation

require multibuild

export_exlib_phases src_unpack src_prepare src_install pkg_postinst

SUMMARY="A Free Anti-Virus and Anti-Spam Filter"
DESCRIPTION="
MailScanner is an email virus scanner, vulnerability protector, and spam tagger.
It supports the Postfix, Sendmail, Exim, Qmail, and ZMailer MTAs, and the Sophos,
McAfee, F-Prot, F-Secure, CommandAV, InoculateIT, Inoculan, eTrust, Kaspersky,
Nod32, AntiVir, BitDefender, RAV, Panda, DrWeb, ClamAV, and other anti-virus
scanners. It uses SpamAssassin for highly successful spam identification, and is
designed to handle denial of service attacks. It will detect password-protected
zip files and apply filename checking to their contents. It is very easy to
install, requires no changes at all to your sendmail.cf file, is designed to be
lightweight, and won't grind your mail system to a halt with its load. It can be
integrated into any email system, regardless of the software in use.
"
HOMEPAGE="http://www.${PN}.info/"
DOWNLOADS="${HOMEPAGE}/files/$(ever major)/tar/${PN}-install-${PV/-p/-}.tar.gz"

#BUGS_TO="philantrop@exherbo.org"
REMOTE_IDS="freecode:${PN}"
UPSTREAM_CHANGELOG="${HOMEPAGE}/ChangeLog"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/documentation.html [[ lang = en ]]"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    clamav          [[ description = [ Use clamav for virus scanning ] ]]
    exim            [[ description = [ Enable the exim integration ] ]]
    postfix         [[ description = [ Enable the postfix integration ] ]]
    sendmail        [[ description = [ Enable the sendmail integration ] ]]
    spamassassin    [[ description = [ Use spamassassin together with MailScanner ] ]]
    ( exim postfix sendmail ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    run:
        dev-lang/perl:*[>=5.10.1]
        dev-perl/Archive-Zip[>=1.30]
        dev-perl/Compress-Raw-Zlib
        dev-perl/Convert-BinHex
        dev-perl/Convert-TNEF[>=0.17]
        dev-perl/DBD-SQLite
        dev-perl/DBI
        dev-perl/Digest-SHA1
        dev-perl/Filesys-Df
        dev-perl/HTML-Parser[>=3.64]
        dev-perl/HTML-Tagset[>=3.03]
        dev-perl/IO-Compress
        dev-perl/IO-stringy[>=2.108]
        dev-perl/MailTools[>=1.50]
        dev-perl/MIME-tools[>=5.417]
        dev-perl/Net-CIDR[>=0.08]
        dev-perl/Net-DNS
        dev-perl/OLE-Storage_Lite[>=0.18]
        dev-perl/PathTools[>=3.30]
        dev-perl/Sys-Hostname-Long
        dev-perl/Sys-SigAction
        dev-perl/TimeDate
        dev-perl/YAML
        net-mail/tnef[>=1.4.3]
        virtual/cron
        clamav? ( app-antivirus/clamav[>=0.88.4] )
        exim? ( mail-mta/exim )
        postfix? ( mail-mta/postfix )
        sendmail? ( mail-mta/sendmail )
        spamassassin? ( mail-filter/spamassassin[>=3.1.5] )
"

WORK=${WORKBASE}/${PNV/-p/-}

MailScanner_src_unpack() {
    default

    unpack ./${PN}-install-${PV/-p*}/perl-tar/${PNV/-p/-}.tar.gz
}

MailScanner_src_prepare() {
    default

    # setup MTA
    RUNASUSER="mail"
    RUNASGROUP="mail"
    SENDMAIL="/usr/sbin/sendmail"
    SENDMAIL2="/usr/sbin/sendmail"

    if option exim ; then
        MTA="exim"
        INQUEUE="/var/spool/exim/input"
        OUTQUEUE="/var/spool/exim/input"
        SENDMAIL="/usr/sbin/exim -oMr MailScanner"
        SENDMAIL2="/usr/sbin/exim -oMr MailScanner"
    elif option postfix ; then
        MTA="postfix"
        RUNASUSER="postfix"
        RUNASGROUP="postfix"
        INQUEUE="/var/spool/postfix/deferred"
        OUTQUEUE="/var/spool/postfix/incoming"
    elif option sendmail ; then
        MTA="sendmail"
        INQUEUE="/var/spool/mqueue.in"
        OUTQUEUE="/var/spool/mqueue"
    fi

    # setup virus scanner(s)
    VIRUS_SCANNERS=""
    option clamav && VIRUS_SCANNERS="clamav ${VIRUS_SCANNERS}"

    if [[ ${VIRUS_SCANNERS} == "" ]]; then
        VIRUS_SCANNERS="none"
        VIRUS_SCANNING="no"
    else
        VIRUS_SCANNING="yes"
    fi

    edo sed -i \
        -e "s/^\(Virus Scanning[ \t]*=\).*/\1 ${VIRUS_SCANNING}/" \
        -e "s/^\(Virus Scanners[ \t]*=\).*/\1 ${VIRUS_SCANNERS}/" \
        "${WORK}"/etc/${PN}.conf

    # setup spamassassin
    if option spamassassin ; then
        edo sed -i \
            -e "s/^\(Use SpamAssassin[ \t]*=\).*$/\1 yes/" \
            "${WORK}"/etc/${PN}.conf
    else
        edo sed -i \
            -e "s/^\(Use SpamAssassin[ \t]*=\).*$/\1 no/" \
            "${WORK}"/etc/${PN}.conf
    fi

    # update bin files
    edo sed -i \
        -e "s#msbindir=/opt/MailScanner/bin#msbindir=/usr/sbin#g" \
        -e "s#config=/opt/MailScanner/etc/MailScanner.conf#config=/etc/${PN}/${PN}.conf#g" \
        "${WORK}"/bin/check_mailscanner

    for each in update_virus_scanners update_phishing_sites update_bad_phishing_sites ; do
        edo sed -i \
            -e "s#/opt/MailScanner/etc#/etc/${PN}#g" \
            -e "s#/opt/MailScanner/bin#/usr/sbin#g" \
            "${WORK}"/bin/${each}
    done

    edo sed -i \
        -e "s#/etc/sysconfig/MailScanner#/etc/conf.d/${PN}#g" \
        "${WORK}"/bin/update_spamassassin
    edo sed -i \
        -e "s#/opt/MailScanner/etc#/etc/${PN}#g" \
        -e "s#/opt/MailScanner/lib#/usr/${LIBDIR}/${PN}#g" \
        -e "s#/opt/MailScanner/bin#/usr/sbin#g" \
        "${WORK}"/bin/${PN}

    # update cron files
    edo sed -i \
        -e "s#/opt/MailScanner/bin/check_mailscanner#/usr/sbin/check_${PN}#g" \
        "${WORK}"/bin/cron/check_${PN}.cron
    edo sed -i \
        -e "s#/opt/MailScanner/bin#/usr/sbin#g" \
        "${WORK}"/bin/cron/update_spamassassin.cron
    edo sed -i \
        -e "s#/opt/MailScanner/bin#/usr/sbin/${PN}_processing_messages_alert#g" \
        "${WORK}"/bin/cron/processing_messages_alert.cron
    edo sed -i \
        -e "s#/opt/MailScanner/bin#/usr/sbin/mailscanner_create_locks#g" \
        "${WORK}"/bin/cron/update_virus_scanners.cron

    for cronfile in update_virus_scanners.cron update_{,bad_}phishing_sites.cron clean.SA.cache.cron; do
    edo sed -i \
        -e "s#/etc/sysconfig/MailScanner#/etc/conf.d/${PN}#g" \
        -e "s#/opt/MailScanner/bin#/usr/sbin#g" \
        "${WORK}"/bin/cron/${cronfile}
    done

    # Determine some things that may need to be changed in conf file
    # (need to arrive at sensible replacement for yoursite)
    YOURSITE=$(dnsdomainname | sed -e "s/\./-/g")

    # ClamAV requires some specific changes to MailScanner.conf
    # when mailscanner is running as root (i.e. sendmail)
    if option clamav ; then
        if [[ ${MTA} == "sendmail" ]] ; then
            WORKGRP="clamav"
            WORKPERM="0640"
        else
            WORKGRP=""
            WORKPERM="0600"
        fi
    else
        WORKGRP=""
        WORKPERM="0600"
    fi

    # update conf files
    edo sed -i \
        -e "s#/opt/MailScanner/etc#/etc/${PN}#g" \
        -e "s#/opt/MailScanner/bin#/usr/sbin#g" \
        -e "s#/opt/MailScanner/lib#/usr/${LIBDIR}/${PN}#g" \
        -e "s#^\(Run As User[ \t]*=\).*#\1 ${RUNASUSER}#" \
        -e "s#^\(Run As Group[ \t]*=\).*#\1 ${RUNASGROUP}#" \
        -e "s#^\(Incoming Queue Dir[ \t]*=\).*#\1 ${INQUEUE}#" \
        -e "s#^\(Outgoing Queue Dir[ \t]*=\).*#\1 ${OUTQUEUE}#" \
        -e "s#^\(MTA[ \t]*=\).*#\1 ${MTA}#" \
        -e "s/^#\(TNEF.*internal\)$/\1/" \
        -e "s/^\(TNEF.*0000\)$/#\1/" \
        -e "s#^\(PID file[ \t]=\).*#\1 /run/${PN}.pid#" \
        -e "s#^\(%org-name%\)[ \t]*=.*#\1 = ${YOURSITE}#" \
        -e "s#^\(Sendmail[ \t]*=\).*#\1 ${SENDMAIL}#" \
        -e "s#^\(Sendmail2[ \t]*=\).*#\1 ${SENDMAIL2}#" \
        -e "s#^\(Incoming Work Group[ \t]*=\).*#\1 ${WORKGRP}#" \
        -e "s#^\(Incoming Work Permissions[ \t]*=\).*#\1 ${WORKPERM}#" \
        -i etc/${PN}.conf \
        -i etc/mailscanner.conf.archives \
        -i etc/mailscanner.conf.with.mcp

    # update spam.assassin.prefs.conf
    edo sed -i -e "s#YOURDOMAIN-COM#${YOURSITE}#" "${WORK}"/etc/spam.assassin.prefs.conf

    # net-mail/clamav net-mail/f-prot package compatibility
    edo sed -i \
        -e "s#/opt/MailScanner/lib#/usr/${LIBDIR}/${PN}#" \
        -e 's#^\(clamav\t.*/usr\)/local$#\1#' \
        -e 's#^\(f-prot.*\)/usr/local/f-prot$#\1/opt/f-prot#' \
        "${WORK}"/etc/virus.scanners.conf

    # update lib files
    edo sed \
        -e "s#/opt/MailScanner/bin#/usr/sbin#g" \
        -e "s#/opt/MailScanner/etc#/etc/${PN}#g" \
        -e "s#/opt/MailScanner/lib#/usr/${LIBDIR}/${PN}#g" \
        -i "${WORK}"/lib/${PN}/ConfigDefs.pl \
        -i "${WORK}"/bin/mailscanner_create_locks \
        -i "${WORK}"/bin/processing_messages_alert
    edo sed -i \
        -e "s#/etc/MailScanner#/etc/${PN}#g" \
        "${WORK}"/lib/${PN}/CustomConfig.pm

    # update rules
    edo sed -i \
        -e "s#/opt/MailScanner/etc#/etc/${PN}#g" \
        "${WORK}"/etc/rules/EXAMPLES

    # finally, change MailScanner.conf into MailScanner.conf.sample
    edo cp "${WORK}"/etc/${PN}.conf "${WORK}"/etc/${PN}.conf.${PV}
    edo mv "${WORK}"/etc/${PN}.conf "${WORK}"/etc/${PN}.conf.sample
}

MailScanner_src_install() {
    exeinto /usr/sbin
    doexe bin/${PN}
    newexe bin/check_mailscanner check_${PN}
    newexe bin/processing_messages_alert ${PN}_processing_messages_alert
    doexe \
        bin/d2mbox \
        bin/df2mbox \
        bin/update_virus_scanners \
        bin/upgrade_MailScanner_conf \
        bin/update_bad_phishing_sites \
        bin/update_phishing_sites \
        bin/analyse_SpamAssassin_cache \
        bin/clean.quarantine \
        bin/clean.SA.cache \
        bin/update_spamassassin \
        bin/upgrade_languages_conf \
        bin/mailscanner_create_locks \
        bin/Quick.Peek
    newexe bin/Sophos.install.linux Sophos.install

    insinto /etc/${PN}
    doins etc/*.conf
    doins etc/mailscanner.conf.with.mcp
    doins etc/mailscanner.conf.archives
    doins etc/${PN}.conf.${PV}
    doins etc/${PN}.conf.sample

    insinto /etc/${PN}/conf.d
    doins etc/conf.d/README

    insinto /etc/${PN}/rules
    doins etc/rules/*
    insinto /etc/${PN}/mcp
    doins etc/mcp/*

    insinto /etc/${PN}
    doins -r etc/reports

    insinto /usr/${LIBDIR}/${PN}
    doins lib/*.prf

    exeinto /usr/${LIBDIR}/${PN}
    doexe lib/*-wrapper
    doexe lib/*-autoupdate
    doexe lib/*-autoupdate.old
    doexe lib/*.pm

    exeinto /usr/${LIBDIR}/${PN}/${PN}
    doexe lib/${PN}/*.pm
    doexe lib/${PN}/*.pl

    exeinto /usr/${LIBDIR}/${PN}/${PN}/CustomFunctions
    doexe lib/${PN}/CustomFunctions/*

    # Set up cron jobs
    exeinto /etc/cron.hourly
    newexe "${WORK}"/bin/cron/check_MailScanner.cron check_${PN}
    for cronfile in update_{virus_scanners,{bad_,}phishing_sites}; do
        newexe "${WORK}"/bin/cron/${cronfile}.cron ${cronfile}
    done

    exeinto /etc/cron.daily
    newexe bin/cron/clean.quarantine.cron clean.quarantine
    newexe bin/cron/clean.SA.cache.cron clean.SA.cache
    newexe bin/cron/processing_messages_alert.cron processing_messages_alert
    newexe bin/cron/update_spamassassin.cron update_spamassassin

    insinto /usr/share/doc/${PNVR}
    doins ${PN}.conf.index.html

    keepdir /var/spool/${PN}/{incoming,quarantine,spamassassin,archive}

    if option postfix ; then
        edo chown -R postfix:postfix "${IMAGE}"/var/spool/${PN}/
    elif option exim ; then
        edo chown -R mail:mail "${IMAGE}"/var/spool/${PN}/
    elif option sendmail ; then
        keepdir /var/spool/mqueue.in
    fi

    if option spamassassin ; then
        dodir /etc/mail/spamassassin
        dosym /etc/${PN}/spam.assassin.prefs.conf /etc/mail/spamassassin/mailscanner.cf
    fi

    emagicdocs
}

MailScanner_pkg_postinst() {
    if [[ -f /etc/${PN}/${PN}.conf ]]; then
        einfo "Upgrading the ${PN}.conf file"
        nonfatal edo cp /etc/${PN}/${PN}.conf /etc/${PN}/${PN}.conf.pre_upgrade.${PV} || eerror "cp ${PN}.conf failed"
        nonfatal edo /usr/sbin/upgrade_MailScanner_conf \
            /etc/${PN}/${PN}.conf.pre_upgrade.${PV} \
            /etc/${PN}/${PN}.conf.${PV} \
            > /etc/${PN}/${PN}.conf || eerror "upgrading ${PN}.conf failed"
    else
        nonfatal edo cp /etc/${PN}/${PN}.conf.sample /etc/${PN}/${PN}.conf || eerror "cp ${PN}.conf.sample failed"
    fi
}

